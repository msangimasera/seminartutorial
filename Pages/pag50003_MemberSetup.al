page 50003 "CORETEC MemberSetup"
{
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = "CORETEC MemberSetup";
    Caption = 'Member Setup';
    InsertAllowed = false;
    DeleteAllowed = false;

    layout
    {
        area(Content)
        {
            group(General)
            {
                field("Member Nos."; "Member Nos.")
                {
                    ApplicationArea = All;
                    Caption = 'Member Nos.';
                }

                field(MinimumAge; MinimumAge)
                {
                    Caption = 'Minimum Age';
                }

                field(MemberType; MemberType)
                {
                    Caption = 'Member Type';
                }
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(ActionName)
            {
                ApplicationArea = All;

                trigger OnAction()
                begin

                end;
            }
        }
    }

    trigger OnOpenPage()
    begin
        if not get then begin
            Init();
            Insert();
        end;
    end;

}