page 50141 "CSD MySeminars"
{
    PageType = ListPart;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = "CSD MySeminar";
    Caption = 'My Seminar';
    layout
    {
        area(Content)
        {
            repeater(Group)
            {
                field("Seminar No."; "Seminar No.")
                {
                }
                field(Name; Seminar.Name) { }
                field(Duration; Seminar."Seminar Duration") { }

                field(Price; Seminar."Seminar Price") { }

            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(ActionName)
            {
                ApplicationArea = All;

                trigger OnAction()
                begin

                end;
            }
        }
    }

    var
        Seminar: Record "CSD Seminar";

    trigger OnOpenPage()
    begin
        SetRange("User Id", UserId);
    end;

    trigger OnAfterGetRecord()
    begin
        if Seminar.Get("Seminar No.") then;
    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        Clear(Seminar);
    end;

    local procedure OpenSeminarCard();
    begin
        if "Seminar No." <> '' then
            Page.Run(Page::"CSD Seminar Card", Seminar);
    end;

}