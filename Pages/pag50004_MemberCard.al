page 50004 "CORETEC MemberCard"
{
    PageType = Card;
    UsageCategory = None;
    SourceTable = "CORETEC Member";

    layout
    {
        area(Content)
        {
            group(MemberDetails)
            {
                field("No."; "No.")
                {
                    ApplicationArea = All;

                }
                field(Name; Name)
                {

                }
                field("Last Date Modified"; "Last Date Modified")
                {

                }
            }
        }

        area(FactBoxes)
        {
            systempart("Links"; Links)
            {

            }

            systempart("Notes"; Notes)
            {

            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(ActionName)
            {
                ApplicationArea = All;

                trigger OnAction()
                begin

                end;
            }
        }
    }

    var
        myInt: Integer;
}