page 50122 "CSD Seminar Registers"
{
    // CSD1.00 - 2018-01-01 - D. E. Veloper
    // Chapter 7 - Lab 2-11

    PageType = List;
    ApplicationArea = All;
    UsageCategory = Lists;
    SourceTable = "CSD Seminar Register";
    Caption = 'Seminary register';
    Editable = false;

    layout
    {
        area(Content)
        {
            repeater(GroupName)
            {
                field("No."; "No.") { }
                field("Creation Date"; "Creation Date") { }
                field("User ID"; "User ID") { }
                field("Source Code"; "Source Code") { }
                field("Journal Batch Name"; "Journal Batch Name") { }

            }
        }
        area(Factboxes)
        {
            systempart("Links"; Links)
            {
            }
            systempart("Notes"; Notes)
            {
            }
        }
    }

    actions
    {
        area(Navigation)
        {
            action("Seminar Ledgers")
            {
                Image = WarrantyLedger;

                RunObject = codeunit "CSD SeminarRegShowLedger";

            }
        }
    }
}