table 50002 "CORETEC MemberSetup"
{
    DataClassification = ToBeClassified;
    Caption = 'MemberSetup';

    fields
    {
        field(1; "Primary Key"; Code[10])
        {

        }

        field(2; "Member Nos."; Code[200])
        {
            DataClassification = ToBeClassified;
            Caption = 'Member Nos.';
            TableRelation = "No. Series";
        }

        field(3; MinimumAge; DateFormula)
        {
            DataClassification = ToBeClassified;
            Caption = 'Minimum Age';
        }

        field(4; MemberType; Option)
        {
            DataClassification = ToBeClassified;
            Caption = 'Member Type';
            OptionMembers = Staff,"Co-Oporate",Individual;
            OptionCaption = 'Staff,Co-Oporate,Individual';
        }
    }

    keys
    {
        key(PK; "Primary Key")
        {
            Clustered = true;
        }
    }

    var
        myInt: Integer;

    trigger OnInsert()
    begin

    end;

    trigger OnModify()
    begin

    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin

    end;

}