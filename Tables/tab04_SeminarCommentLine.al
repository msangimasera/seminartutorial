table 50004 "CSD Seminar Comment Line"
{
    // CSD1.00 - 2018-01-01 - D. E. Veloper
    // Chapter 5 - Lab 2-1
    // Chapter 7 - Lab 3-2

    DataClassification = ToBeClassified;
    Caption = 'Seminar Comment Line';
    DrillDownPageId = "CSD Seminar Comment List";

    fields
    {
        field(1; TableName; Option)
        {
            Caption = 'Table Name';
            OptionMembers = Seminar,"Seminar Registration","Posted Seminar Registration";
        }
        field(2; DocumentLineNo; Integer)
        {
        }
        field(3; No; Code[20])
        {
            Caption = 'No.';
            TableRelation = if (TableName = const (Seminar)) "CSD Seminar"
            else
            if (TableName = const ("Seminar Registration")) "CSD Seminar Reg. Header"
            else
            if (TableName = const ("Posted Seminar Registration")) "CSD Posted Seminar Reg. Header";
        }

        field(4;
        LineNo;
        Integer)
        {
            Caption = 'Line No.';
        }

        field(5; Date; Date)
        {
        }

        field(6; Code; Code[10])
        {

        }

        field(7; Comment; Text[80])
        {
            Caption = 'Comment';
        }

    }
    keys
    {
        key(PK; TableName, DocumentLineNo, LineNo)
        {
            Clustered = true;
        }
    }

}