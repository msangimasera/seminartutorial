table 50003 "CORETEC Member"
{
    DataClassification = ToBeClassified;
    Caption = 'Member';
    DrillDownPageId = 50005;
    LookupPageId = 50005;

    fields
    {
        field(1; "No."; Code[20])
        {
            DataClassification = ToBeClassified;
        }
        field(2; Name; Text[50])
        {
            DataClassification = ToBeClassified;
            Caption = 'Member Name';
        }

        field(3; "Last Date Modified"; Date)
        {
            DataClassification = ToBeClassified;
            Editable = false;
        }

        field(4; UserId; Code[50])
        {
            DataClassification = ToBeClassified;
            Editable = false;
        }

        field(5; "No. Series"; Code[10])
        {
            Editable = false;
            Caption = 'No. Series';
            TableRelation = "No. Series";
        }

    }

    keys
    {
        key(PK; "No.")
        {
            Clustered = true;
        }
    }

    var
        MemberSetup: Record "CORETEC MemberSetup";
        NoSeriesMgt: Codeunit NoSeriesManagement;

    trigger OnInsert()
    begin
        if "No." = '' then begin
            MemberSetup.Get;
            MemberSetup.TestField("Member Nos.");
            NoSeriesMgt.InitSeries(MemberSetup."Member Nos.", xRec."No. Series", 0D, "No.", "No. Series");
        end;
        UserId := USERID;
    end;

    trigger OnModify()
    begin
        "Last Date Modified" := Today;
    end;

    trigger OnDelete()
    begin
        Error('You are not allowed to delete a member!!');
    end;

    trigger OnRename()
    begin
        "Last Date Modified" := Today;
    end;

}