report 50102 "CORETEC VendorStatement"
{
    UsageCategory = ReportsAndAnalysis;
    ApplicationArea = All;
    RDLCLayout = './Layouts/VendorStatement.rdl';
    Caption = 'Vendor Statement';
    DefaultLayout = RDLC;

    dataset
    {
        dataitem(Vendor; Vendor)
        {
            RequestFilterFields = "No.";
            DataItemTableView = sorting ("No.");
            CalcFields = "Balance (LCY)";

            column(No_; "No.") { IncludeCaption = true; }
            column(VendorName; Name) { IncludeCaption = true; }
            column(Post_Code; "Post Code") { IncludeCaption = true; }
            column(Phone_No_; "Phone No.") { IncludeCaption = true; }
            column(Balance__LCY_; "Balance (LCY)") { IncludeCaption = true; }

            dataitem("Vendor Ledger Entry"; "Vendor Ledger Entry")
            {
                DataItemTableView = sorting ("Entry No.");
                DataItemLink = "Vendor No." = field ("No.");
                CalcFields = Amount;

                column(Posting_Date; "Posting Date") { IncludeCaption = true; }
                column(Document_No_; "Document No.") { IncludeCaption = true; }
                column(Description; Description) { IncludeCaption = true; }
                column(Amount; Amount) { IncludeCaption = true; }

            }

        }

        dataitem("Company Information"; "Company Information")
        {
            column(CompanyName; Name) { }
        }
    }

    labels
    {
        VendorStatementCap = 'Vendor Statement';
    }




}